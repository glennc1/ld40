local TileMapRenderSystem = tiny.system(class "TileMapRenderSystem")


local spriteSheet = love.graphics.newImage("assets/AsteroidTiles.png")
spriteSheet:setFilter('nearest', 'nearest')

function drawQuad(spriteIndex, x, y)
  local spriteSize = 16
  -- Expensive todo move out of update loop
  spriteQuad = love.graphics.newQuad(spriteIndex * spriteSize, 0, spriteSize, spriteSize, spriteSheet:getDimensions())
  love.graphics.draw(spriteSheet, spriteQuad, x * spriteSize, y * spriteSize)
end

local map = {
  { 1, 2, 1, 1, 2, 1 },
  { 2, 2, 1, 1, 1, 2 },
  { 1, 1, 2, 1, 2, 1 },
  { 2, 2, 1, 1, 1, 2 }
}

function TileMapRenderSystem:update(dt)
  
  for y = 1, 4 do
    for x = 1, 6 do
      drawQuad(map[y][x], x - 1, y - 1)
    end
  end
  
end



return TileMapRenderSystem