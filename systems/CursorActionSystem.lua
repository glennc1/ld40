local CursorActionSystem = tiny.processingSystem(class "CursorActionSystem")

CursorActionSystem.filter = tiny.requireAll("movementRange")

function CursorActionSystem:init(cursor)
  self.cursor = cursor
end

function CursorActionSystem:preProcess(dt)
  self.currentUnit = nil
end

function CursorActionSystem:process(e, dt)

  -- if the cursor is over a unit
  if self.cursor.pos.x == e.pos.x and self.cursor.pos.y == e.pos.y then
    self.currentUnit = e
  end
  
  if self.cursor.selectedUnit ~= nil then
    local unit = self.cursor.selectedUnit.pos
    local target = self.cursor.pos
    local hdiff = target.x - unit.x
    local vdiff = target.y - unit.y
    local elbow = { x = unit.x + hdiff, y = unit.y }
    love.graphics.line(unit.x * 16 + 8, unit.y * 16 + 8, elbow.x * 16 + 8, elbow.y * 16 + 8)
    love.graphics.line(elbow.x * 16 + 8, elbow.y * 16 + 8, target.x * 16 + 8, target.y * 16 + 8)
  end
  
end

local spriteSheet = love.graphics.newImage("assets/AsteroidTiles.png")
spriteSheet:setFilter('nearest', 'nearest')
local driveBlip = love.audio.newSource("assets/sndDrive.wav", "static")

function CursorActionSystem:postProcess(dt)
  
  if self.cursor.selectPressed then
    if self.cursor.selectedUnit == nil then
      -- select new
      self.cursor.selectedUnit = self.currentUnit
    elseif self.currentUnit == nil then
      --move selected
      if self.cursor.targetBuilding == nil then
        self.cursor.selectedUnit.animationPath = getPath(self.cursor.pos, self.cursor.selectedUnit.pos)
        self.cursor.selectedUnit.pos.x = self.cursor.pos.x
        self.cursor.selectedUnit.pos.y = self.cursor.pos.y
        driveBlip:play()
      else
        local mine = {
          pos = {x = self.cursor.pos.x, y = self.cursor.pos.y },
          spriteIndex = self.cursor.targetBuilding,
        }
        world:add(mine)
      end
      self.cursor.selectedUnit = nil
      self.cursor.targetBuilding = nil
    elseif self.cursor.selectedUnit == self.currentUnit then
      -- deselect by clicking self
      self.cursor.selectedUnit = nil
      self.cursor.targetBuilding = nil
    else
      -- select other entity
      self.cursor.selectedUnit = self.currentUnit
    end
    self.cursor.selectPressed = false
  end
  
  local selectedUnit = self.cursor.selectedUnit
  
  if self.cursor.buildPressed then
    if selectedUnit ~= nil and self.cursor.targetBuilding == nil then
      self.cursor.targetBuilding = {{ x = 0,  y = 1 }, { x = 0,  y = 2 }, { x = 1,  y = 2 }}
    else
      self.cursor.targetBuilding = nil
    end
    self.cursor.buildPressed = false
  end
  
  if selectedUnit ~= nil then
    love.graphics.setColor(255, 0, 0, 100)
    love.graphics.rectangle("fill", selectedUnit.pos.x * 16, selectedUnit.pos.y * 16, 16, 16)
    love.graphics.setColor(255, 255, 255)
  end
  
  if self.cursor.targetBuilding ~= nil then
    local spriteSize = 16
    -- Expensive todo move out of update loop
    spriteQuad = love.graphics.newQuad(self.cursor.targetBuilding[1].x * spriteSize, self.cursor.targetBuilding[1].y * spriteSize, spriteSize, spriteSize, spriteSheet:getDimensions())
    love.graphics.draw(spriteSheet, spriteQuad, self.cursor.pos.x * spriteSize, self.cursor.pos.y * spriteSize)
  end
  
end

function getPath(to, from)
  local dx = to.x - from.x
  local dy = to.y - from.y
  local xStep = to.x > from.x and 1 or -1
  local yStep = to.y > from.y and 1 or -1
  local path = {}
  for x=0, dx, xStep do
    table.insert(path, { x = from.x + x, y = from.y })
  end
  for y=yStep, dy, yStep do
      table.insert(path, { x = to.x, y = from.y + y })
  end
  return path
end

return CursorActionSystem