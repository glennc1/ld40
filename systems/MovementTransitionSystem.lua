local MovementTransitionSystem = tiny.processingSystem(class "MovementTransitionSystem")

MovementTransitionSystem.filter = tiny.requireAll("animationPath")


local tilesPerSec = 4
function MovementTransitionSystem:process(e, dt)
  local path = e.animationPath
  if path ~= nil then
    e.animationTravelled = e.animationTravelled + tilesPerSec * dt
    if e.animationTravelled + 1 >= #path then
      e.animationPath = nil
      e.animationTravelled = 0.0
    end
  end
end

return MovementTransitionSystem