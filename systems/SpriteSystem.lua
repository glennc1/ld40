local SpriteSystem = tiny.processingSystem(class "SpriteSystem")

SpriteSystem.filter = tiny.requireAll("pos")


local spriteSheet = love.graphics.newImage("assets/AsteroidTiles.png")
spriteSheet:setFilter('nearest', 'nearest')

local animationOffset = 0
local accumulator = 0.0
local animationInterval = 0.666

function SpriteSystem:preProcess(dt)
  accumulator = accumulator + dt
  
  while accumulator >= animationInterval do
    animationOffset = animationOffset + 1
    accumulator = accumulator - animationInterval
  end
  
end

function SpriteSystem:process(e, dt)
  local spriteSize = 16
  local x, y = getRenderCoordinates(e, dt)
  local orient = getOrientation(e)
  -- Expensive todo move out of update loop
  local animationIndex = math.fmod(animationOffset,#e.spriteIndex) + 1
  if e.animationPath ~= nil then
    -- don't bob while animating
    animationIndex = 1
  end
  local spriteIndex = e.spriteIndex[animationIndex]

  spriteQuad = love.graphics.newQuad((spriteIndex.x + orient) * spriteSize, spriteIndex.y * spriteSize, spriteSize, spriteSize, spriteSheet:getDimensions())
  love.graphics.draw(spriteSheet, spriteQuad, x * spriteSize, y * spriteSize)
end

function getRenderCoordinates(e)
  local x, y = e.pos.x, e.pos.y
  if e.animationPath ~= nil then
    local cur = e.animationPath[math.floor(e.animationTravelled) + 1]
    local nex = e.animationPath[math.ceil(e.animationTravelled) + 1]
    local offset = e.animationTravelled - math.floor(e.animationTravelled)
    x, y = cur.x + (nex.x - cur.x) * offset, cur.y + (nex.y - cur.y) * offset
  end
  return x, y
end

function getOrientation(e)
  local orient = 0
  if e.animationPath ~= nil then
    local cur = e.animationPath[math.floor(e.animationTravelled) + 1]
    local nex = e.animationPath[math.ceil(e.animationTravelled) + 1]
    orient = nex.x > cur.x and 0 or nex.x < cur.x and 1 or nex.y > cur.y and 2 or 3
  end
  return orient
end

return SpriteSystem