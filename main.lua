tiny = require("lib.tiny")
class = require("lib.30log")

local cursor = {
  pos = {x = 1, y = 1},
  spriteIndex = {{x = 0, y = 0}, {x = 1, y = 3}}, 
  isCursor = true,
  selectPressed = false,
  buildPressed = false,
  selectedUnit = nil,
  targetBuilding = nil
}

local rover = {
  pos = {x = 3, y = 2},
  spriteIndex = {{x = 4, y = 0}, {x = 6, y = 1}},
  movementRange = 3,
  animationPath = {},
  animationTravelled = 0.0
}

local truck = {
  pos = {x = 5, y = 2},
  spriteIndex = {{x = 3, y = 0}, {x = 7, y = 1}},
  movementRange = 1, 
  animationPath = {},
  animationTravelled = 0.0
}

world = tiny.world(
  require("systems.TileMapRenderSystem")(), 
  require("systems.MovementTransitionSystem")(), 
  require("systems.SpriteSystem")(), 
  require("systems.CursorActionSystem")(cursor),
  cursor,
  rover,
  truck
)

function love.load()
  if arg[#arg] == "-debug" then require("mobdebug").start() end
  io.stdout:setvbuf("no")
  
  music = love.audio.newSource("assets/ambient.wav")
  music:play()
  music:setLooping(true)
end

function love.update(dt)
end
 
function love.draw()
  love.graphics.scale(8, 8)
  
  local dt = love.timer.getDelta()
  world:update(dt)
  love.graphics.print('Hello World!', 400, 300)
end

local cursorBlip = love.audio.newSource("assets/sndCursorBlip.wav", "static")

function love.keypressed(key)
  if key == 'a' then
    cursor.pos.x = cursor.pos.x - 1
  elseif key == 'd' then
    cursor.pos.x = cursor.pos.x + 1
  elseif key == 'w' then
    cursor.pos.y = cursor.pos.y - 1
  elseif key == 's' then
    cursor.pos.y = cursor.pos.y + 1
  elseif key == 'space' then
    cursor.selectPressed = true
  elseif key == 'b' then
    cursor.buildPressed = true
  end
  cursorBlip:play()
end
  
  